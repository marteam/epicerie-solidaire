import {Command, CommandType, Event, EventType, ValidationResult} from "./engine.model";
import uuid from "uuid/v4";
import {Product} from "./base";

export class AddProductCommand implements Command {
  type = CommandType.ADD_PRODUCT;

  constructor(public payload: Omit<Product, "id">) {}

  validate(): ValidationResult {
    return {};
  }
}

export class ProductAddedEvent implements Event {
  date = new Date();
  id = uuid();
  type = EventType.PRODUCT_ADDED_EVENT;

  constructor(public payload: Omit<Product, "id">) {}
}
