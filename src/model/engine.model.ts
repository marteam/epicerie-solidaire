import {AddProductCommand} from "./product.model";

export enum EventType {
  PRODUCT_ADDED_EVENT = "PRODUCT_ADDED_EVENT",
  PRODUCT_UPDATED_EVENT = "PRODUCT_UPDATED_EVENT"
}

export interface Event {
  id: string;
  type: EventType;
  date: Date;
  payload: object;
}

export enum CommandType {
  ADD_PRODUCT = "ADD_PRODUCT",
  MODIFY_PRODUCT = "MODIFY_PRODUCT"
}

export interface ValidationResult {
  errors?: CommandError[];
}

export interface Command {
  readonly type: CommandType;
  payload: object;

  validate(): ValidationResult;
}

export function cloneCommand(serializedCommand: Command): Command {
  switch (serializedCommand.type) {
    case CommandType.ADD_PRODUCT:
      return new AddProductCommand((serializedCommand as AddProductCommand).payload);
    default:
      throw "A command type seems to have been forgotten in cloneCommand.";
  }
}

export interface CommandError {}
