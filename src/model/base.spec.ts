import {displayPriceInEuros} from "./base";

describe("base", () => {
  describe(".displayPriceInEuros", () => {
    it("should convert from cents to euros and append € symbol", () => {
      expect(displayPriceInEuros(123)).toEqual("1.23 €");
    });
    it("should display the 0 before the dot if price is under 1€", () => {
      expect(displayPriceInEuros(98)).toEqual("0.98 €");
    });
  });
});
