import {Product} from "./base";

export interface ProductsStore {
  [key: string]: Product;
}

export interface Store {
  products: ProductsStore;
}
