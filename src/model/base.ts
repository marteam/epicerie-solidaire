export type PriceInCents = number;

export interface Product {
  id: string;
  name: string;
  description?: string;
  // category ?
  commercialValue: PriceInCents;
  contribution: PriceInCents;
}

export function displayPriceInEuros(priceInCents: PriceInCents): string {
  const significantZeroIfNeeded = priceInCents < 100 ? "0" : "";
  return `${significantZeroIfNeeded}${priceInCents.toString().replace(/\d{2}$/, ".$&")} €`;
}
