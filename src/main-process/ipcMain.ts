import {ipcMain} from "electron";
import {loadStore} from "./store";
import {cloneCommand, Command, Event} from "../model/engine.model";
import {initApplicationState} from "./engine/application-state";
import {Store} from "../model/store.model";
import IpcMainEvent = Electron.IpcMainEvent;

function initApp() {
  const { engine, projectionUpdaters, projections } = initApplicationState().startEngine();

  const events: Event[] = loadStore();
  events.forEach(event =>
    projectionUpdaters.forEach(projectionUpdater => projectionUpdater.updateWith(event))
  );

  return { engine, projectionUpdaters, projections };
}

ipcMain.on("get", (event: IpcMainEvent, entityName: keyof Store) => {
  if (!entityName) {
    event.returnValue = projections;
  } else {
    event.returnValue = projections[entityName];
  }
});

ipcMain.on("dispatch", (event: IpcMainEvent, jsonCommand: Command) => {
  // here we need to rebuild the command class instance, since passing through the IPC implies converting to JSON, then losing class methods.
  event.returnValue = engine.process(cloneCommand(jsonCommand));
});

const { engine, projections } = initApp();
