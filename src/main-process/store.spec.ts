import {ProductAddedEvent} from "../model/product.model";
import {toJson} from "./store";

describe("store", () => {
  describe(".toJson", () => {
    it("should be able to convert a string representation of events to Json", () => {
      const payloads = [
        { name: "milk x6", commercialValue: 200, contribution: 50 },
        { name: "apple juice", commercialValue: 100, contribution: 10 },
        { name: "meatloaf", commercialValue: 600, contribution: 60 }
      ];
      const eventsAsString = payloads
        .map(payload => JSON.stringify(new ProductAddedEvent(payload)) + "\n")
        .join("");

      const eventStore = toJson(eventsAsString);
      expect(eventStore).toHaveLength(3);
      expect(eventStore[0].payload).toMatchObject(payloads[0]);
      expect(eventStore[1].payload).toMatchObject(payloads[1]);
      expect(eventStore[2].payload).toMatchObject(payloads[2]);
    });
  });
});
