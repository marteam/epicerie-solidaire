import * as fs from "fs";
import os from "os";
import {Event} from "../model/engine.model";

/**
 * data file contains a stack of Events (Json format), one per line.
 * When reading the data, events are formatted to be a valid JSON (separated by commas, with square brackets around)
 */

const DATA_FOLDER_PATH = `${os.homedir()}/episol`;
const DATA_FILE_PATH = `${DATA_FOLDER_PATH}/data.json`;

export function toJson(eventsAsString: string) {
  console.log();
  try {
    return JSON.parse(
      `[${eventsAsString
        .substr(0, eventsAsString.length - 1) // remove the last \n
        .split("\n")
        .join(",\n")}]`
    );
  } catch (e) {
    console.error("An error occured while reading file", e, "\n", eventsAsString);
  }
}

export function loadStore(): Event[] {
  console.log("Loading data from", DATA_FILE_PATH);
  try {
    const eventsAsString = fs.readFileSync(DATA_FILE_PATH, { encoding: "utf8" });
    return toJson(eventsAsString);
  } catch (error) {
    if (fs.existsSync(DATA_FILE_PATH)) {
      console.error(error);
      throw "Data file exists but is not readable";
    }
    // else
    console.log("No data found ; gonna create it.");
    const events: Event[] = [];
    if (!fs.existsSync(DATA_FOLDER_PATH)) {
      fs.mkdirSync(DATA_FOLDER_PATH);
    }
    fs.writeFileSync(DATA_FILE_PATH, "");
    return events;
  }
}

export interface WriteError {
  cause: any;
}

export function persistEvent(event: Event): WriteError | null {
  try {
    fs.appendFileSync(DATA_FILE_PATH, JSON.stringify(event) + "\n", { encoding: "utf8" });
    return null;
  } catch (error) {
    console.error("An error occured while add event", error);
    return { cause: error };
  }
}
