import {AddProductCommand} from "../../model/product.model";
import {initApplicationState} from "./application-state";
import {EventType} from "../../model/engine.model";

describe("engine", () => {
  it("processes ADD_PRPODUCT event", () => {
    const productPayload = {
      name: "Milk",
      commercialValue: 400,
      contribution: 40
    };

    const { engine, projections } = initApplicationState().startEngine();

    const addProductEvent = engine.process(new AddProductCommand(productPayload));
    expect(addProductEvent).toMatchObject({
      type: EventType.PRODUCT_ADDED_EVENT,
      payload: productPayload
    });
    expect(Object.entries(projections.products)).toHaveLength(1);
    expect(projections.products[addProductEvent.id]).toMatchObject(productPayload);
  });
});
