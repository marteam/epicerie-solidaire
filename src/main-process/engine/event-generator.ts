import {Command, CommandType, Event, EventType} from "../../model/engine.model";
import uuid from "uuid/v4";

export function generateEvent(command: Command): Event {
  switch (command.type) {
    case CommandType.ADD_PRODUCT:
      return {
        id: uuid(),
        type: EventType.PRODUCT_ADDED_EVENT,
        date: new Date(),
        payload: command.payload
      };
    default:
      throw `Command type unhandled in generateEvent: ${command.type}`;
  }
}
