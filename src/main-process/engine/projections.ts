import {Event, EventType} from "../../model/engine.model";
import {Product} from "../../model/base";
import {ProductAddedEvent} from "../../model/product.model";
import {Store} from "../../model/store.model";

export interface ProjectionUpdater<T = object> {
  updateWith(event: Event): void;
}

export class ProductsProjectionUpdater implements ProjectionUpdater<Product> {
  constructor(private projections: Store) {}

  updateWith(event: Event): void {
    switch (event.type) {
      case EventType.PRODUCT_ADDED_EVENT:
        const product: Product = {
          ...(event as ProductAddedEvent).payload,
          id: event.id
        };
        Object.assign(this.projections.products, { [event.id]: product });
        break;
    }
  }
}
