# How does the engine work?

The engine processes commands as follows:

- validate the command: this step include local validation (check mandatory fields of valid prices for instance), but can also include business rules as "Is the _recipient_ still active"
- generate an event
- persist the event
- update projections

## Command

A command is a action : its name starts with a verb: ADD_PRODUCT for instance.

## Event

An event is something appended ; its name contains a verb in a past tense: PRODUCT_ADDED for instance.

Events are the only source of thruth of the app.

## Projections

Those are representations of the data that simplify the querying ; it may look like a database.
