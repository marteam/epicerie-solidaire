import {Command, Event} from "../../model/engine.model";
import {persistEvent} from "../store";
import {ProjectionUpdater} from "./projections";
import {generateEvent} from "./event-generator";

export interface Engine {
  projectionUpdaters: ProjectionUpdater[];

  process(command: Command): Event;
}

export class EngineImpl implements Engine {
  constructor(public projectionUpdaters: ProjectionUpdater[] = []) {}

  process(command: Command) {
    const validationResult = command.validate();
    if (validationResult.errors) {
      // TODO: throw something
    }

    const event: Event = generateEvent(command);

    persistEvent(event);

    this.projectionUpdaters.forEach(projectionUpdater => projectionUpdater.updateWith(event));

    return event;
  }
}
