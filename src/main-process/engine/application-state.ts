import {ProductsProjectionUpdater, ProjectionUpdater} from "./projections";
import {Engine, EngineImpl} from "./engine";
import {Store} from "../../model/store.model";

export class ApplicationStateIgnition {
  projections: Store = { products: {} };

  private projectionUpdaters: ProjectionUpdater[] = [
    new ProductsProjectionUpdater(this.projections)
  ];

  startEngine(): ApplicationState {
    const engine = new EngineImpl(this.projectionUpdaters);
    return new ApplicationState(engine, this.projections, this.projectionUpdaters);
  }
}

class ApplicationState {
  constructor(
    public engine: Engine,
    public projections: Store,
    public projectionUpdaters: ProjectionUpdater[]
  ) {}
}

export function initApplicationState() {
  return new ApplicationStateIgnition();
}
