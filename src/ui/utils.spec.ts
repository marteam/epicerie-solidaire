import {sortBy} from "./utils";
import {Product} from "../model/base";

interface ReducedProduct {
  name: string;
}

const appleJuice: ReducedProduct = { name: "apple juice" };
const milk: ReducedProduct = { name: "milk" };

describe("sortBy", () => {
  it("should put Milk after Apple juice", () => {
    expect(
      sortBy<ReducedProduct>("name", [appleJuice, milk])
    ).toEqual([appleJuice, milk]);
  });

  it("should not consider initial order", () => {
    expect(
      sortBy<ReducedProduct>("name", [milk, appleJuice])
    ).toEqual([appleJuice, milk]);
  });

  it("should sort elements alphabetically, not being case sensitive", () => {
    const CHOCOLATE_UPPER_CASE: ReducedProduct = { name: "CHOCOLATE" };
    expect(
      sortBy<ReducedProduct>("name", [milk, appleJuice, CHOCOLATE_UPPER_CASE])
    ).toEqual([appleJuice, CHOCOLATE_UPPER_CASE, milk]);
  });

  const potatoes: Product = { id: "1", name: "potatoes", contribution: 1, commercialValue: 4 };
  const oranges: Product = { id: "2", name: "oranges", contribution: 2, commercialValue: 7 };
  const rice: Product = { id: "3", name: "rice", contribution: 0, commercialValue: 1 };

  it("should deal with any object type", () => {
    expect(
      sortBy<Product>("name", [potatoes, oranges, rice])
    ).toEqual([oranges, potatoes, rice]);
  });

  it("should deal with any string field", () => {
    expect(
      sortBy<Product>("id", [rice, oranges, potatoes])
    ).toEqual([potatoes, oranges, rice]);
  });

  it("should deal with any field type", () => {
    expect(
      sortBy<Product>("commercialValue", [rice, oranges, potatoes])
    ).toEqual([rice, potatoes, oranges]);
  });
});
