/**
 * the following declaration enable to call window.ipcRenderer without Typescript yelling at us
 */
import IpcRenderer = Electron.IpcRenderer;

declare global {
  interface Window {
    ipcRenderer: IpcRenderer;
  }
}

import {Command, CommandError, Event} from "../model/engine.model";
import {Store} from "../model/store.model";

export function dispatch(command: Command): Event | CommandError {
  return window.ipcRenderer.sendSync("dispatch", command);
}

export function getStore<T>(projection: keyof Store): T {
  return window.ipcRenderer.sendSync("get", projection) as T;
}
