import React from "react";
import styled, {css} from "styled-components";

const style = css`
  display: block;
  width: 100%;
  height: auto;
`;

export const InputField = styled.input`
  ${style}
`;

export const TextArea = styled.textarea`
  ${style}
`;
