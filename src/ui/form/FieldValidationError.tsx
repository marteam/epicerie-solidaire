import React from "react";
import {FieldRenderProps} from "react-final-form-hooks";
import styled from "styled-components";

const StyledError = styled.span`
  color: red;
`;

interface FieldValidationErrorProps {
  field: FieldRenderProps;
}

/**
 * Displays the possible error message for the given field
 * @param field
 * @constructor
 */
const FieldValidationError = ({ field }: FieldValidationErrorProps) =>
  field.meta.touched && field.meta.error ? (
    <StyledError>{field.meta.error}</StyledError>
  ) : null;

export default FieldValidationError;
