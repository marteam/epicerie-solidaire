import React from "react";
import {displayPriceInEuros, Product} from "../../model/base";
import {Card} from "react-bootstrap";
import styled from "styled-components";

function capitalize(value: string): string {
  return value && value.charAt(0).toUpperCase() + value.slice(1);
}

const StyledCardBody = styled(Card.Body)`
  // to override default style
  &.card-body {
    padding: 10px;
    display: grid;
    grid-template-columns: 150px 1fr;
    gap: var(--grid-gap);
  }

  > * {
    margin: 0;
  }
  > h5,
  hr {
    grid-column-start: 1;
    grid-column-end: 3;
    margin: 0;
  }
`;

const Description = styled.div`
  grid-column-start: 1;
  grid-column-end: 3;
  font-style: italic;
`;

const AttributeLabel = styled.label`
  font-weight: bold;
  display: block;
`;

interface Props {
  product: Product;
}

const ProductCard = ({ product: { name, description, contribution, commercialValue } }: Props) => (
  <Card bg="light">
    <StyledCardBody>
      <Card.Title as="h5">{capitalize(name)}</Card.Title>
      {description && <Description>{description}</Description>}
      <hr />
      <AttributeLabel>Valeur marchande</AttributeLabel>
      <span>{displayPriceInEuros(commercialValue)}</span>
      <AttributeLabel>Participation</AttributeLabel>
      <span>{displayPriceInEuros(contribution)}</span>
    </StyledCardBody>
  </Card>
);

export default ProductCard;
