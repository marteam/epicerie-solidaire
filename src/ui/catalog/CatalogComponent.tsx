import React, {useState} from "react";
import ProductForm from "./ProductForm";
import {Product} from "../../model/base";
import styled from "styled-components";
import {Button} from "react-bootstrap";
import ProductCard from "../product/ProductCard";
import {dispatch} from "../ipcRenderer";
import {AddProductCommand} from "../../model/product.model";
import {useCatalog} from "./ProductsCatalogContext";
import {removeDiacritics, sortBy} from "../utils";

const Container = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: 1fr 350px;

  > h1 {
    grid-column: 1 / 3;
  }
  > :not(h1) {
    padding: 10px;
  }
`;

const CardsWrapper = styled.div`
  padding: 1rem 0;
  display: grid;
  grid-template-columns: repeat(auto-fit, 330px);
  gap: var(--grid-gap);
`;

const H1 = styled.h1`
  text-align: center;
  background-color: var(--primary);
  color: white;
  padding-bottom: 0.5rem;
`;

const ProductsListContainer = styled.div``;

const ActionBar = styled.div`
  display: flex;
  align-items: center;
  > input {
    margin-right: 1rem;
  }
`;

const CatalogComponent = () => {
  const { products, loadCatalog } = useCatalog();
  const [showForm, setShowForm] = useState(false);
  const [filterRegExp, setFilterRegExp] = useState<RegExp>();

  const productsAsArray: Product[] = sortBy<Product>("name", Object.values(products));

  return (
    <Container>
      <H1>Catalogue</H1>

      <ProductsListContainer>
        <h5>
          {productsAsArray.length >= 2
            ? `${productsAsArray.length} produits disponibles`
            : `${productsAsArray.length} produit disponible`}
        </h5>
        <ActionBar>
          <input
            placeholder="Rechercher"
            onChange={event =>
              setFilterRegExp(new RegExp(removeDiacritics(event.target.value), "i"))
            }
          />
          <Button onClick={() => setShowForm(true)} variant="outline-primary">
            Ajouter
          </Button>
        </ActionBar>

        <CardsWrapper>
          {productsAsArray
            .filter(p => !filterRegExp || removeDiacritics(p.name).match(filterRegExp))
            .map(product => (
              <ProductCard key={product.id} product={product} />
            ))}
        </CardsWrapper>
      </ProductsListContainer>

      <div id="form">
        {showForm ? (
          <ProductForm
            onSubmit={(product: Product) => {
              dispatch(new AddProductCommand(product));
              loadCatalog();
              return Promise.resolve();
            }}
          />
        ) : null}
      </div>
    </Container>
  );
};

export default CatalogComponent;
