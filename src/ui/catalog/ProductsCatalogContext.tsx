import React, {useContext, useEffect, useState} from "react";
import {getStore} from "../ipcRenderer";
import {ProductsStore} from "../../model/store.model";

const ProductsCatalogContext = React.createContext<{
  products: ProductsStore;
  loadCatalog(): void;
}>({ products: {}, loadCatalog() {} });

export const CatalogProvider: React.FC = ({ children }) => {
  const [products, setProductsList] = useState<ProductsStore>({});

  function loadCatalog() {
    const loadedCatalog = getStore("products") as ProductsStore;
    setProductsList(loadedCatalog);
  }

  useEffect(() => loadCatalog(), []);
  return (
    <ProductsCatalogContext.Provider value={{ products, loadCatalog }}>
      {children}
    </ProductsCatalogContext.Provider>
  );
};

export function useCatalog() {
  return useContext(ProductsCatalogContext);
}

export default ProductsCatalogContext;
