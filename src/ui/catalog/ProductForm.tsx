import React, {useEffect, useRef} from "react";
import {Product} from "../../model/base";
import {ValidationErrors} from "final-form";
import {useField, useForm} from "react-final-form-hooks";
import FieldValidationError from "../form/FieldValidationError";
import uuid from "uuid/v4";
import {Button, Container, Form} from "react-bootstrap";
import {InputField, TextArea} from "../form/InputField";

type ProductFormValues = Partial<Product>;

function validate(values: ProductFormValues): ValidationErrors {
  const errors: ValidationErrors = {};
  if (!values.name) {
    errors.name = "Valeur requise";
  }
  if (!values.commercialValue) {
    errors.commercialValue = "valeur requise";
  }
  if (!values.contribution) {
    errors.contribution = "valeur requise";
  }
  return errors;
}

interface ProductFormProps {
  onSubmit(product: Product): Promise<void>;
}

const ProductForm = ({ onSubmit }: ProductFormProps) => {
  const firstFieldRef = useRef<HTMLInputElement>(null);
  const { form, handleSubmit, valid, submitting } = useForm({
    onSubmit: (values: Product) => {
      const product: Product = {
        ...values,
        id: uuid(),
        contribution: values.contribution * 100,
        commercialValue: values.commercialValue * 100
      };
      onSubmit(product).then(() => {
        if (firstFieldRef && firstFieldRef.current) {
          firstFieldRef.current.focus();
        }
        form.reset();
      });
    },
    validate
  });
  const productName = useField("name", form);
  const description = useField("description", form);
  const commercialValue = useField("commercialValue", form);
  const contribution = useField("contribution", form);

  useEffect(() => {
    if (firstFieldRef && firstFieldRef.current) {
      firstFieldRef.current.focus();
    }
  }, []);

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <h5>Nouveau produit</h5>
        <Form.Group>
          <Form.Label htmlFor={productName.input.name}>Nom du produit</Form.Label>
          <InputField {...productName.input} ref={firstFieldRef} />
          <FieldValidationError field={productName} />
        </Form.Group>

        <Form.Group>
          <Form.Label htmlFor={description.input.name}>Description</Form.Label>
          <TextArea {...description.input} rows={2} />
        </Form.Group>
        <Form.Group>
          <Form.Label htmlFor={commercialValue.input.name}>Valeur marchande (en €)</Form.Label>
          <InputField {...commercialValue.input} type="number" />
          <FieldValidationError field={commercialValue} />
        </Form.Group>
        <Form.Group>
          <Form.Label htmlFor={contribution.input.name}>Participation (en €)</Form.Label>
          <Form.Control {...contribution.input} type="number" />
          <FieldValidationError field={contribution} />
        </Form.Group>
        <Button type="submit" disabled={!valid || submitting} variant="primary" color="primary">
          Enregistrer
        </Button>
      </Form>
    </Container>
  );
};

export default ProductForm;
