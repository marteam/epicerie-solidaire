import * as React from "react";
import * as ReactDOM from "react-dom";
import "./styles.css";
import {CatalogProvider} from "./catalog/ProductsCatalogContext";
import CatalogComponent from "./catalog/CatalogComponent";
import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  return (
    <CatalogProvider>
      <CatalogComponent />
    </CatalogProvider>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
